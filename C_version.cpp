#include <cmath>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

class molecule {
private:
    // information about energy levels
    int _numLvls;
    vector<double> _energies;
    vector<int> _weights;
    // information about radiative transitions
    int _numRadTrans;
    vector<pair<int,int> > _transitions; //up,down
    vector<double> _frequencies;
    double** _EinsteinA;
    double** _EinsteinB;
    // information about collissional transitions
    int _numCollPartners;
    vector<double> _partnerFractions;
    vector<int> _numCollTrans;
    vector<int> _numTemps;
    double** _temperatures;
    double**** _EinsteinC; // partner, temp, up, down

public:

    molecule() : _numLvls(0), _numRadTrans(0), _EinsteinA(nullptr), _EinsteinB(nullptr), _EinsteinC(nullptr){}

    ~molecule() {
        for (int c=0; c<_numCollPartners;c++){
            for (int t=0; t<_numTemps[c]; t++){
                for (int i=0; i<_numLvls; i++){
                    delete[] _EinsteinC[c][t][i];
                }
                delete[] _EinsteinC[c][t];
            }
            delete[] _EinsteinC[c];
            delete[] _temperatures[c];
        }
        delete[] _EinsteinC;
        delete[] _temperatures;

        for (int l=0; l <_numLvls; l++){
            delete[] _EinsteinA[l];
            delete[] _EinsteinB[l];
        }
        delete[] _EinsteinA;
        delete[] _EinsteinB;
    }

    void setNumLvls(int value){
        _numLvls = value;
        if (_numLvls > 0) {
            _EinsteinA = new double*[_numLvls];
            _EinsteinB = new double*[_numLvls];
            for (int l=0; l <_numLvls; l++){
                _EinsteinA[l] = new double[_numLvls];
                _EinsteinB[l] = new double[_numLvls];
                for (int i=0; i<_numLvls;i++){
                    _EinsteinA[l][i] = 0;
                    _EinsteinB[l][i] = 0;
                }
            }
        }
    }
    int numLvls(){
        return _numLvls;
    }

    void addLevel(double energie, int weight){
        _energies.push_back(energie);
        _weights.push_back(weight);
    }

    double energy(int index){
        return _energies[index];
    }

    int weight(int index){
        return _weights[index];
    }

    void setNumRadTrans(int value){
        _numRadTrans = value;
    }

    int numRadTrans(){
        return _numRadTrans;
    }

    void addRadTrans(int up, int down, double frequency, double EinsteinA){
        pair<int, int> trans(up,down);
        _transitions.push_back(trans);
        _frequencies.push_back(frequency);
        _EinsteinA[up][down] = EinsteinA;
        // compute B-coefficient via Einstein relations
        // Bij = coeff for stimulated emission, Bji = coeff for extinction (j<i)
        double h = 4.135667516e-15; //eV*s
        double h_si = 6.626e-34; //J*s
        double c = 299792458.0; //m/s
        _EinsteinB[up][down] = EinsteinA * pow(c,3) / (2*h_si * pow(frequency,3)); //m2/J
        _EinsteinB[down][up] = _EinsteinB[up][down] * _weights[up] / _weights[down];
    }

    pair<int,int> transition(int index){
        return _transitions[index];
    }

    double frequency(int index){
        return _frequencies[index];
    }

    double frequency(int up, int down){
        for (int t=0; t<_numRadTrans; t++){
            if ((_transitions[t].first == up) && (_transitions[t].second == down)) {
                return _frequencies[t];
            }
        }
        return 0;
    }

    double EinsteinA(int up, int down){
        return _EinsteinA[up][down];
    }

    double EinsteinB(int up, int down){
        return _EinsteinB[up][down];
    }

    void setNumPartners(int value){
        _numCollPartners= value;
        if (_numCollPartners >0){
            _EinsteinC = new double***[_numCollPartners];
        }
        _temperatures = new double*[_numCollPartners];
    }

    int numCollPartners(){
        return _numCollPartners;
    }

    void setNumTemps(int value, int partner){
        _numTemps.push_back(value);
        // allocate T array
        _temperatures[partner] = new double[value];
        // allocate collission C array
        if (_numTemps[partner] > 0 && _numLvls > 0) {
            _EinsteinC[partner] = new double**[_numTemps[partner]];
            for (int t=0; t < _numTemps[partner]; t++){
                _EinsteinC[partner][t] = new double*[_numLvls];
                for (int i=0; i<_numLvls; i++){
                    _EinsteinC[partner][t][i] = new double[_numLvls];
                    for (int j=0; j<_numLvls; j++){
                        _EinsteinC[partner][t][i][j] = 0;
                    }
                }
            }
        }
    }

    int numTemps(int partner) {
        return _numTemps[partner];
    }

    void setTemp(double value, int partner, int index){
        _temperatures[partner][index] = value;
    }

    void addPartnerFraction(double value){
        _partnerFractions.push_back(value);
    }

    void addNumCollTrans(int value){
        _numCollTrans.push_back(value);
    }

    int numCollTrans(int partner){
        return _numCollTrans[partner];
    }

    void setCollTrans(int partner, int tempIndex, int up, int down,  double EinsteinC){
        double k_b = 8.617e-5; //eV/K
        _EinsteinC[partner][tempIndex][up][down] = EinsteinC;
        //calculate the inverse coefficient via LTE relation
         _EinsteinC[partner][tempIndex][down][up] = _EinsteinC[partner][tempIndex][up][down]
                 * exp(-(_energies[up]-_energies[down])/(k_b*_temperatures[partner][tempIndex])) * _weights[up]/_weights[down];
    }

};

// read data for the molecule from LAMDA database file
int read_file(molecule* molec) {

    double h = 4.135667516e-15; // eV*s
    double c = 299792458.0; // m/s

    // open file
    ifstream file("CO.txt", std::ifstream::in);
    if (!file.is_open()){
        cout << "Could not open the data file \n";
        return 1;
    }

    // read molecule info
    string line;
    for (int skip=0;skip<4;skip++) {
        getline(file, line);
    }
    // number of energy levels
    getline(file, line);
    int numLvls;
    file >> numLvls;
    molec->setNumLvls(numLvls);
    cout << molec->numLvls() << '\n';
    getline(file, line);
    getline(file, line);
    // read energy levels: energy, weight
    double energy;
    int weight;
    int lvl;
    for (int l=0; l<numLvls;l++){
        file >> lvl;
        file >> energy;
        file >> weight;
        getline(file, line);
        molec->addLevel(energy*c * h/0.01,weight); // E[cm-1 -> eV]
        cout << "energy " << molec->energy(l)<< " eV \t g " << molec->weight(l) << '\n';
    }

    getline(file, line);
    // number of radiative transistions
    int num_trans;
    file >> num_trans;
    getline(file, line);
    molec->setNumRadTrans(num_trans);
    cout << "number of radiative transitions " << molec->numRadTrans() << '\n';
    getline(file, line);
    // read transistions: upper lvl, lower lvl, A-coefficient, frequency
    int up;
    int down;
    double Acoeff;
    double freq;
    for (int t=0;t<num_trans;t++){
        file >> lvl;
        file >> up >> down >> Acoeff >> freq;
        getline(file, line);
        up = up-1;
        down = down -1;
        freq = freq * 1e6; //Hz
        molec->addRadTrans(up,down,freq,Acoeff);
        cout << "Freq " << molec->frequency(t)  << "Hz \t Aij "
             << molec->EinsteinA(up,down) << " s-1 \t Bij "
             << molec->EinsteinB(up,down) << " m2/J \t Bji "
             << molec->EinsteinB(down,up) << " m2/J\n";
    }

    getline(file, line);
    // number of collision partners in the data file
    int numpartners=0;
    file >> numpartners;
    getline(file, line);
    molec->setNumPartners(numpartners);
    cout << "number of collision partners " << molec->numCollPartners() << '\n';

    // loop over partners
    int numcoll=0;
    int numtemps =0;
    for (int p=0; p<numpartners; p++){
        cout << "partner " << p+1 << " out of " << numpartners << '\n';
        getline(file, line);
        getline(file, line);

        cout << "data for collisions: " << line << '\n';
        getline(file, line);
        file >> numcoll;
        getline(file, line);
        molec->addNumCollTrans(numcoll);
        cout << "number of collisional transitions " << molec->numCollTrans(p) << '\n';
        // read collission coefficients
        getline(file, line);

        file >> numtemps;
        molec->setNumTemps(numtemps,p);
        cout << "number of temperatures " << molec->numTemps(p) << '\n';
        getline(file, line);
        // read temperature values
        getline(file, line);
        double temp;
        for (int t=0;t<numtemps;t++) {
            file >> temp;
            molec->setTemp(temp,p,t); //K
            //cout << "temp " << temp << '\n';
        }
        getline(file, line);
        // read transitions for all temperatures: upper lvl, lower lvl, C-coefficient for each temp
        getline(file, line);
        int num;
        double einsteinC;
        for (int c=0; c<molec->numCollTrans(p);c++) {
            file >> num;
            file >> up;
            file >> down;
            //cout << num << " " << up << " " << down << " ";
            for (int t=0;t<numtemps;t++) {
                file >> einsteinC;
                //cout << einsteinC << " ";
                molec->setCollTrans(p,t,up-1,down-1,einsteinC); //cm^3 s^-1
            }
            getline(file, line);

            //cout << '\n';
        }
        //cout << '\n';
    }

    cout << "done\n";
    file.close();

    return 0;
}

int main(int argc, char *argv[])
{
    // read file
    molecule* molec = new molecule;
    if (read_file(molec)==1){
        delete molec;
        return 1;
    }
    double _temperature = 100;
    vector<double> pops;
    double Z=0;
    double k_b = 8.617e-5; // eV/K
    //1.38064852e-23;// m2 kg s-2 K-1
    //calc partition function
    for(int i=0;i<molec->numLvls();i++){
        cout << molec->energy(i)/(k_b*_temperature) << '\n';
        Z += molec->weight(i) * exp((-1)*molec->energy(i)/(k_b*_temperature));
        cout << Z << '\n';

    }
    cout << Z << '\n';
    for(int i=0;i<molec->numLvls();i++){
        pops.push_back(molec->weight(i) * exp(-molec->energy(i)/(k_b*_temperature)) / Z);
        cout << "pop " << i << ": " << pops[i] << '\n';
    }
    delete molec;
    return 0;
}


/**i
h = 4.135667516e-15 #eV*s
h_si = 6.626e-34 #J*s
c = 299792458.0 #m/s
k_b = 8.617e-5 #eV/K


# calculate the netto collision coefficient for a gas consisting of different components at temp T
def calc_total_C(num_lvls,C_all,temps_all,T,comp_fractions):
    print "Calculating C_ij for temperature", T, "and component mixture", comp_fractions
    C = np.zeros((num_lvls,num_lvls))
    # loop over collission partners
    for p in range(len(comp_fractions)):
        max_index = len(temps_all[p])
        if T <= temps_all[p][0]:
            print "Component",p,": using temperature", temps_all[p][0]
            for i in range(num_lvls):
                for j in range(num_lvls):
                    C[i][j] = C[i][j] + comp_fractions[p] * C_all[p][0][i][j]
        elif T >= temps_all[p][max_index-1]:
            print "Component",p,": using temperature", temps_all[p][max_index-1]
            for i in range(num_lvls):
                for j in range(num_lvls):
                    C[i][j] = C[i][j] + comp_fractions[p] * C_all[p][max_index-1][i][j]
        else:
            # determine temperature entries needed to interpolate
            t = 1
            while temps_all[p][t] < T:
                t = t+1
            t_frac = (temps_all[p][t] - T)/(temps_all[p][t] - temps_all[p][t-1])
            print "Component",p,": interpolating between temperatures", temps_all[p][t-1], "and", temps_all[p][t], "with fraction",t_frac
            for i in range(num_lvls):
                for j in range(num_lvls):
                    C[i][j] = C[i][j] + comp_fractions[p] * (t_frac*C_all[p][t-1][i][j] + (1-t_frac)*C_all[p][t][i][j])
    return C

# calculate LTE occupation numbers with partition function method
def calc_ni_partion(T, num_lvls, g, E):
    ni = []
    Z=0.0
    #calc partition function
    for i in range(0,num_lvls):
        Z = Z + g[i]*math.exp(-E[i]/(k_b*T))
    for i in range(0,num_lvls):
        ni.append(g[i]*math.exp(-E[i]/(k_b*T)) / Z)
    return ni

# calculate LTE occupation numbers by solving the system of coupled balance equations
def calc_ni_set(num_lvls, C):
    # solve  A*n = 0
    # n = [n1,n2,...ni,...,nn]

    # fill matrix A
    A = np.zeros((num_lvls,num_lvls))
    for i in range(0, num_lvls):
        for j in range(0,num_lvls):
            A_ij = 0
            if i==j:
                for l in range(0, num_lvls):
                    if l != i:
                        A_ij = A_ij - C[i][l]
            else:
                A_ij = C[j][i]
            A[i][j] = A_ij

    # solve A*n=0 with svd
    # A = U*S*V.T
    U, S, Vt = np.linalg.svd(A)
    #print S
    sol=[]
    # loop over the singular values (elements of diagonal matrix s)
    # a column of V_j is a solution to the equation Ax=0 if it's corresponding S_jj = 0
    for s in range(0,num_lvls):
        if S[s] <1e-20 :
            #print Vt[s]
            sol.append(Vt[s])
    print "Found", len(sol),"solution(s)"
    #s = np.diag(s)
    #print np.dot(np.dot(u,s),vh)[0:1][0:1]
    #print vh
    #print np.dot(coef,sol.T)

    # sol can be multiplied by a constant factor: normalise to sum(sol_i) = 1
    # this is only the case here (collissional transitions only) because the set of equations 
    # is independent of the density of the collision partners (constant factor can be divided out) 
    # (assumes all values are either positive or neg)
    for s in range(len(sol)):
        norm=np.sum(sol[s])
        sol[s]=sol[s]/norm

    return sol

# calculate non-LTE occupation numbers by solving the system of coupled balance equations (A and C)
# density is the total number density of collision partners
def calc_ni_setAC(num_lvls, A, C, density):
    # solve  M*n = 0
    # n = [n1,n2,...ni,...,nn]

    # fill matrix M
    M = np.zeros((num_lvls,num_lvls))
    for a in range(0, num_lvls):
        for b in range(0,num_lvls):
            M_ab = 0
            # upper triagle
            if b>a:
                M_ab = A[b][a] + density*C[b][a]
            # diagonal
            elif a==b:
                for j in range(0, b):
                    M_ab = M_ab - A[b][j] - density*C[b][j]
                for j in range(a+1, num_lvls):
                    M_ab = M_ab - density*C[b][j]
            else:
                M_ab = density*C[b][a]
            M[a][b] = M_ab

    # solve A*n=0 with svd
    # A = U*S*V.T
    U, S, Vt = np.linalg.svd(M)
    print S
    # loop over the singular values (elements of diagonal matrix s)
    # a column of V_j is a solution to the equation Ax=0 if it's corresponding S_jj = 0
    # REMARK: s values given in decending order???
    #for s in range(0,num_lvls):
    #    if S[s] <1e-15:
    #        #print Vt[s]
    #        sol.append(Vt[s])
    if S[num_lvls-1] < 1.0e-4*S[num_lvls-2]:
        print "Found good solution"
    else:
        print "WARNING: unreliable solution"
    sol = Vt[num_lvls-1]
    #s = np.diag(s)
    #print np.dot(np.dot(u,s),vh)[0:1][0:1]
    #print vh
    #print np.dot(coef,sol.T)

    # sol can be multiplied by a constant factor: normalise to sum(sol_i) = 1
    # this is only the case here (collissional transitions only) because the set of equations 
    # is independent of the density of the collision partners (constant factor can be divided out) 
    # (assumes all values are either positive or neg)
    #for s in range(len(sol)):
    norm=np.sum(sol)
    sol=sol/norm

    return sol

# calculate non-LTE occupation numbers by solving the system of coupled balance equations (A B and C)
# density is the total number density of collision partners
# rad_field is the incomming radiation field
def calc_ni_setABC(num_lvls, A, B, C, density, rad_field):
    # solve  M*n = 0
    # n = [n1,n2,...ni,...,nn]

    # fill matrix M
    M = np.zeros((num_lvls,num_lvls))
    for a in range(0, num_lvls):
        for b in range(0,num_lvls):
            M_ab = 0
            # upper triagle
            if b>a:
                M_ab = A[b][a] + B[b][a]*rad_field[b][a] + density*C[b][a]
            # diagonal
            elif a==b:
                for j in range(0, b):
                    M_ab = M_ab - A[b][j] - (B[b][j]*rad_field[b][j]) - density*C[b][j]
                for j in range(a+1, num_lvls):
                    M_ab = M_ab - B[b][j]*rad_field[j][b] - density*C[b][j]
            else:
                M_ab = B[b][a]*rad_field[a][b] + density*C[b][a]
            M[a][b] = M_ab

    # solve A*n=0 with svd
    # A = U*S*V.T
    U, S, Vt = np.linalg.svd(M)
    #print S
    # loop over the singular values (elements of diagonal matrix s)
    # a column of V_j is a solution to the equation Ax=0 if it's corresponding S_jj = 0
    # REMARK: s values given in decending order???
    #for s in range(0,num_lvls):
    #    if S[s] <1e-15:
    #        #print Vt[s]
    #        sol.append(Vt[s])
    if S[num_lvls-1] > 1.0e-4*S[num_lvls-2]:
        #print "Found good solution"
    #else:
        print "WARNING: unreliable solution"
    sol = Vt[num_lvls-1]
    #s = np.diag(s)
    #print np.dot(np.dot(u,s),vh)[0:1][0:1]
    #print vh
    #print np.dot(coef,sol.T)

    # sol can be multiplied by a constant factor: normalise to sum(sol_i) = 1
    # (assumes all values are either positive or neg)
    norm=np.sum(sol)
    sol=sol/norm

    return sol


# MAIN
density = 1.0e3 #particles/cm3t
pH2=0.66 #fraction para H2
oH2=0.33 #fraction ortho H2
T = 10.0

num_lvls, g, E, A, freq, num_partners, temps_all, C_all, B = readData()
component_fractions = [pH2,oH2]

C = calc_total_C(num_lvls,C_all,temps_all,T,component_fractions)
#print C

tot_p=0
tot_s=0
tot_AC=0
tot_ABC=0
ni_part = calc_ni_partion(T, num_lvls, g, E)
sols = calc_ni_set(num_lvls, C)
if len(sols)>1:
    "WARNING: more than one solutions found for the occupation levels!"
ni_set = sols[0]
solsAC = calc_ni_setAC(num_lvls, A, C, density)
#if len(solsAC)==0:
#    "WARNING: no solutions found for the occupation levels!"
ni_setAC = solsAC

#rad_field = np.zeros((num_lvls,num_lvls))
rad_field = np.ones((num_lvls,num_lvls))
rad_field = rad_field * 1.0e-10 # eV/(m2 s)

solABC = calc_ni_setABC(num_lvls, A, B, C, density, rad_field)

for i in range(0,num_lvls):
    print "lvl\t", i, ":\t" , round(ni_part[i],8), '\t', round(ni_set[i],8), '\t' , round(ni_setAC[i],8), '\t' , round(solABC[i],8)
    tot_p=tot_p+ni_part[i]
    tot_s=tot_s+ni_set[i]
    tot_AC=tot_s+ni_setAC[i]
    tot_ABC=tot_ABC+solABC[i]
print "total", tot_p, tot_s, tot_AC, tot_ABC

#for rad in range(10000):
#    rad_field = rad_field * 1.0e-13 * float(rad)
#    solABC = calc_ni_setABC(num_lvls, A, B, C, density, rad_field)

*/


