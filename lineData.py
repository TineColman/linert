'''
    LINE RADIATIVE TRANSFER

    file: lineData.py
    Description: Class to stores the transitions data for a specified atom or molecule
    Author: Tine Colman
'''

import numpy as np
import os
import copy

from constants import *

SUPPORTED_SPECIES = ['HCO+', 'H13CO+', 'N2H+', 'SiO', 'HNC', 'HCN', 'CO']

class LineData:

    def __init__(self, species, skip_C=False, verbose=True):
        self.species = None       # name of the atom or molecule
        self.mu = 0               # molecular weight
        self.num_lvls = 0         # number of energy levels
        self.E = None             # energy of each level [eV]
        self.g = None             # weight of each level
        self.freq = None          # Frequency freq[i][j] of transition i<->j [Hz]
        self.A = None             # Einstein A-coeff A[i][j] for transition i->j [1/s]
        self.B = None             # Einstein B-coeff B[i][j] for transition i->j [m2/(eV*s)]
        self.C_all = None         # collision coeffs C_all[p][T] for each coll partner [m3/s]
        self.num_partners = 0     # number of collision partners in the data file 
        self.temps_all = None     # T at which C is given for each coll partner [K]

        filename = self.get_filename(species)
        self._read_data(filename, skip_C, verbose)

    ''' construct the filename for a given species '''
    def get_filename(self, species):
        # filename is already given
        if (species[-4:]=='.dat') or (species[-4:]=='.txt'):
            return species

        # pick a file from the folder LAMDA included in this package
        THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
        database = os.path.join(THIS_FOLDER, 'LAMDA')
        if species in SUPPORTED_SPECIES:
            filename = os.path.join(database, species+'.dat')
        else:
            print('Unknow species. Chose from', SUPPORTED_SPECIES, 'or provide a LAMDA datafile.')
            exit()
        return filename

    ''' read LAMBA data file with atom/molecule info and set the corresponding class attributes
        transistions between i----
                             j----
        PARAMS:
          filename = name of the LAMBDA file
          skip_C   = flag to not read the collision data and C coeffs
          verbose  = flag to print info to screen while processing file
        '''
    def _read_data(self, filename, skip_C, verbose):
        f = open(filename,'r')
        f.readline()
        self.species = f.readline()
        print('Reading data for', self.species)
        # molecular weight
        f.readline()
        self.mu = float(f.readline())
        # number of energy levels
        f.readline()
        self.num_lvls = int(f.readline())
        if verbose:
            print('number of energy levels:', self.num_lvls)
        # read energy levels: energy E, statistical weight g
        f.readline()
        E = []
        g = []
        for l in range(self.num_lvls):
            words =  f.readline().split()
            E.append(float(words[1]) * 100.*c_si*h_ev) # cm^-1 -> eV
            g.append(float(words[2]))
            if verbose:
                print('energy {:.8f} eV    weight {:3.0f}'.format(E[l], g[l]))
        self.E = np.array(E)
        self.g = np.array(g)
    
        # number of radiative transistions
        f.readline()
        num_trans = int(f.readline())
        if verbose:
            print('number of radiative transitions', num_trans)
        # read transistions: upper lvl, lower lvl, Einstein A-coefficient, frequency
        f.readline()
        A = np.zeros((self.num_lvls, self.num_lvls))
        freq = np.zeros((self.num_lvls, self.num_lvls))
        for t in range(num_trans):
            words = f.readline().split()
            i = int(words[1]) - 1
            j = int(words[2]) - 1
            A[i][j] = float(words[3]) # s^-1
            freq[i][j] = float(words[4]) * 1e9 # GHz -> Hz
            freq[j][i] = freq[i][j]
            if verbose:
                print('{:3d}->{:3d} at {:e} Hz with A = {:e} s^-1'.format(i, j,freq[i][j], A[i][j]))
        self.freq = freq
        self.A = A
    
        # compute Einstein B-coefficient via Einstein relations
        # Bij = coeff for stimulated emission, Bji = coeff for extinction (j<i)
        B = np.zeros((self.num_lvls, self.num_lvls))
        for i in range(0,self.num_lvls):
            for j in range(0,i):
                if A[i][j] != 0:
                    B[i][j] = A[i][j] * (c_si**2) / (2*h_ev * (freq[i][j])**3) # m2/(eV*s)
                    B[j][i] = B[i][j] * g[i]/g[j]
        self.B = B

        if skip_C:
            f.close()
            return

        # number of collision partners in the data file
        f.readline()
        self.num_partners = int(f.readline())
        if verbose:
            print('number of collision partners', self.num_partners)
        C_all = []
        temps_all = []
        for partner in range(self.num_partners):
            # reference
            f.readline()
            line = f.readline()
            if verbose:
                print('data for collisions', line[2:-1])
            # number of collisional transitions
            f.readline()
            num_collis = int(f.readline())
            if verbose:
                print('number of collisional transitions', num_collis)
            # number of temperatures in the table
            f.readline()
            num_temps = int(f.readline())
            if verbose:
                print('number of temperatures', num_temps)
            # read the temperature values
            f.readline()
            words = f.readline().split()
            temps = np.zeros(num_temps)
            for t in range(num_temps):
                temps[t] = float(words[t])
            temps_all.append(temps) #K
    
            # read collision coeff data: upper lvl, lower lvl, C-coefficient for each temp
            C = np.zeros((num_temps, self.num_lvls, self.num_lvls))
            f.readline()
            for col in range(num_collis):
                words = f.readline().split()
                i = int(words[1]) - 1
                j = int(words[2]) - 1
                for t in range(num_temps):
                    C[t][i][j] = float(words[3+t]) * 1.e-6 # cm3/s -> m3/s
            # calculate the inverse coefficient via LTE relation
            for i in range(self.num_lvls):
                for j in range(i):
                    for t in range(num_temps):
                        if C[t][i][j] != 0:
                            C[t][j][i] = C[t][i][j] * np.exp(-(E[i]-E[j])/(kb_ev*temps[t]))*g[i]/g[j]
            # add collision partner data to global array
            C_all.append(C)

        self.C_all = np.array(C_all)
        self.temps_all = np.array(temps_all)

        f.close()

    ''' Calculate net collision coeff for a gas consisting of different components at temp T
        Interpolates table between T values
        PARAMS:
          T = temperature (K)
          comp_fractions = fraction of the total density in each component 
        RETRUN:
          C = netto collision coeff C[i][j] (m3/s) '''
    def calc_total_C(self, T, comp_fractions, verbose=True):
        #if verbose:
        #    print('Calculating C_ij for temperature', T, 'and component mixture', comp_fractions)
        if len(comp_fractions) != self.num_partners:
            print('ERROR: the number of elements in comp_fractions does not match the number of partners in the read data!')
            exit()

        C = np.zeros((self.num_lvls,self.num_lvls))
        for p in range(self.num_partners):
            # check if T is at the limit of the table values
            max_index = len(self.temps_all[p]) - 1
            if (T <= self.temps_all[p][0]) or (T >= self.temps_all[p][max_index]):
                if T <= self.temps_all[p][0]: # T is at lower edge
                    index = 0
                else:
                    index = max_index # T is at top edge
                for i in range(self.num_lvls):
                    for j in range(self.num_lvls):
                        C[i][j] = C[i][j] + comp_fractions[p] * self.C_all[p][index][i][j]
                if verbose and ((T < self.temps_all[p][0]) or (T > self.temps_all[p][max_index])):
                    print('WARNING: Temperature outside of collision coefficient table ranges for collision component', p, '!')
                    print('Using C coeff at temperature', self.temps_all[p][index], 'instead of', T)
            # interpolate between table entries
            else:
                t = 1 # T index of upper limit
                while self.temps_all[p][t] < T:
                    t = t+1
                t_frac = (self.temps_all[p][t] - T)/(self.temps_all[p][t] - self.temps_all[p][t-1])
                #if verbose:
                #    print('Component', p, ': interpolating between temperatures',\
                #       self.temps_all[p][t-1], 'and', self.temps_all[p][t], '(t_frac=', t_frac, ')')
                for i in range(self.num_lvls):
                    for j in range(self.num_lvls):
                        interpol = (1-t_frac) * self.C_all[p][t][i][j] + \
                                       t_frac * self.C_all[p][t-1][i][j]
                        C[i][j] = C[i][j] + comp_fractions[p] * interpol
                        #if verbose:
                        #    print(self.C_all[p][t-1][i][j], self.C_all[p][t][i][j], '->', C[i][j],\
                        #          '(t_frac=', t_frac, ')')
        return C

    ''' Limit the data to the first few levels (these are the most relevant ones in most cases).
        The new number of levels, new_num_lvls, should be chosen based on an LTE estimate.
        A new LineData object is returned rather than overriding the current one. '''
    def reduce_linedata(self, new_num_lvls):
        ld = copy.deepcopy(self)
        ld.num_lvls = new_num_lvls
        ld.E = ld.E[:new_num_lvls]
        ld.g = ld.g[:new_num_lvls]
        ld.freq = ld.freq[:new_num_lvls,:new_num_lvls]
        ld.A = ld.A[:new_num_lvls,:new_num_lvls]
        ld.B = ld.B[:new_num_lvls,:new_num_lvls]
        if ld.C_all is not None:
            ld.C_all = ld.C_all[:,:,:new_num_lvls,:new_num_lvls]
        return ld

#----- Some tests ----------------------------------------------------------------

def test_hco():
    line = LineData('HCO+')
    line.calc_total_C(12.5, [1.])
    line.calc_total_C(125.0, [1.])
    line.calc_total_C(2000., [1.])
    line.calc_total_C(2500., [1.])
    line.calc_total_C(5., [1.])
    line.calc_total_C(10., [1.])

def test_co():
    line = LineData('CO', skip_C=True)
    line.calc_total_C(10., [.25,0.75])

if __name__=='__main__':
    # TODO could use some proper test cases
    test_hco()
    test_co()
