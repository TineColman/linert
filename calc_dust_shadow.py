# simple script to calculate the shadow of a dust cloud
# Takes a background source, put the cloud in from of it and calculates the extinction
# plots the resulting image

import numpy as np
from matplotlib import pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.collections import PatchCollection
from mpl_toolkits.axes_grid1 import make_axes_locatable

from constants_lineRT import *
from get_density import *

def rho_shell(r, p, Mtot, R):
    A = 1.5*Mtot/(4*np.pi)/(R**1.5)
    if r>R:
        return 0
    else:
        return A * r**(-p)

def get_kappa(wavelength):
    return 0 

# calc the optical depth for each pixel along the line of sight
def calc_optical_depth(kappa, rho, dx):
    alpha = kappa * rho
    tau = np.sum(alpha, axis=0) * dx
    return tau

def dust_shadow():
    # sed background
    wavelengths = [3.55, 4.44, 7.87, 23.68, 70., 160., 250., 350., 500., 870.] # micron
    SED_background = [5.13272944e-18, #W/m2 micron
    3.65441023e-18,
    2.65229773e-17,
    9.88593945e-16,
    5.42549027e-16,
    4.36738465e-16,
    6.45974738e-17,
    1.50578588e-17,
    2.58597896e-18,
    2.03943315e-20]

    # extinction coeff
    kappa_1 = 8.801e2 #cm2/g
    kappa_1 = kappa_1*0.01**2/0.001 # m/kg

    # toy model density
    #nx = 2**6
    #box_size = 7.5*PC #m
    #dx = box_size/nx
    #rho =  get_simple_density(nx)
    #print np.max(rho)

    # a small box
    rho, nx, dx = load_box2(8)
    print np.max(rho)
    fig2, axes2 = plt.subplots(nrows=1, ncols=1, figsize=(8, 8))
    imag2 = axes2.imshow(np.sum(rho, axis=0), interpolation='none', cmap='Greys_r', aspect='equal', origin='lower')#, vmin=-27, vmax=-22)
    divider = make_axes_locatable(axes2)
    cax = divider.append_axes('right', size='5%', pad=0.05)
    cbar = fig2.colorbar(imag2, cax=cax)
    cbar.solids.set_rasterized(True)
    cbar.set_label('cloud')
    #plt.show()
    plt.savefig('density.png')

    # correct input lum for num pixels
    lum_px = SED_background[0] / (nx*nx)

    # optical depth
    tau = calc_optical_depth(kappa_1, rho, dx)

    # image
    initial_image = np.full((nx,nx), lum_px)
    final_image = initial_image * np.exp(-tau)

    # plot image
    final_image = np.log10(final_image)
    fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(8, 8))
    imag = axes.imshow(final_image, interpolation='none', cmap='Greys_r', aspect='equal', origin='lower')#, vmin=-27, vmax=-22)
    divider = make_axes_locatable(axes)
    cax = divider.append_axes('right', size='5%', pad=0.05)
    cbar = fig.colorbar(imag, cax=cax)
    cbar.solids.set_rasterized(True)
    cbar.set_label('log(F_lambda)')
    #plt.show()
    plt.savefig('test1.png')

if __name__=='__main__':
    dust_shadow()
