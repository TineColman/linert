import pynbody
import numpy as np
from matplotlib import pyplot as plt

from lineData import LineData
import populations
import rt_functions
from constants import *

def get_line_data(method, species):
    if method=='LTE_partition':
        line = LineData(species, skip_C=True, verbose=False)
    else:
        line = LineData(species, verbose=False)
    # reduce the number of relevant levels (should be estimated properly by checking LTE lvlpops)
    line = line.reduce_linedata(5)
    return line

def get_number_densities(method, selection):
    if method=='LTE_partition':
        n_coll = np.zeros(Ncells)
    else:
        print('TODO: get number of H2, are there other collision partners?')
        n_coll = selection['rho'].in_units('kg m**-3') / (2*MH)
    n_molec = 1.e-4 * selection['rho'].in_units('kg m**-3') / (2*MH)
    return n_molec, n_coll

def get_lvlpops(method, line, up, down, comp_fractions=None):
    T = selection['temperature'].in_units('K')
    Ncells = len(T)
    x_up = np.zeros(Ncells)
    x_down = np.zeros(Ncells)

    if method=='LTE_partition':
        for cell in range(Ncells):
            lvlpops = populations.calc_lvlpops_partion(T[cell], line.num_lvls, line.g, line.E)
            x_up[cell] = lvlpops[up]
            x_down[cell] = lvlpops[down]
            print(lvlpops)
    elif method=='LTE_coll':
        if comp_fraction==None:
            print('ERROR: no comp_fractions given!')
            exit()
        for cell in range(Ncells):
            C = line.calc_total_C(T[cell], comp_fractions)
            lvlpops = populations.calc_lvlpops_coll(line.num_lvls, C)
            x_up[cell] = lvlpops[up]
            x_down[cell] = lvlpops[down]
            print(lvlpops)
    else:
        print('Unsupported method for calculating level populations:', method, '. Exiting...')
        exit()

    return x_up, x_down

def make_CO_mock(selection, method):
    species = 'CO'
    up = 1
    down = 0
    #comp_fracs = [0.66, 0.33] # para H2 and ortho H2

    # load the line data
    line = get_line_data(method, species)

    # get number densities of the species and the collision partners
    n_molec, n_coll = get_number_densities(method, selection)

    # calc level pops in every cell assuming LTE
    x_up, x_down = get_lvlpops(method, line, up, down)

    # calc emission
    emis_tot = rt_functions.integrated_emissivity(line.freq[up][down], x_up, n_molec, line.A[up][down])
    print(emis_tot)
    selection['emis_CO'] = emis_tot
    selection['emis_CO'].units = 'J s^-1 m^-3' #sr^-1

    # make image
    pynbody.plot.sph.image(data, qty='emis_CO', units='J s^-1 m^-2', width='500 au')
    plt.savefig('test.png')

if __name__ == '__main__':

    # choose method for determining level populations
    method = 'LTE_partition'

    # load ramses data and select part of the box
    path = "./"
    out = 1
    data = pynbody.load("{}output_{:05}/".format(path, out))
    data['pos'] -= (0.125e-3, 0.125e-3, 0.125e-3) #kpc
    selection = data.g[pynbody.filt.Sphere('2000 au', [0, 0, 0])]
    Ncells = len(selection['rho'])
    print('Ncells', Ncells)
    selection['temperature'] = 10
    selection['temperature'].units = 'K'

    # do the CO mock observation
    make_CO_mock(selection, method)


