import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

from constants_lineRT import *
from get_density import *


''' Plot the image '''
def plot_image(image, outname):
    final_image = np.ma.log10(image)
    fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(8, 8))
    imag = axes.imshow(final_image.filled(np.min(final_image)), interpolation='none', cmap='Greys_r', aspect='equal', origin='lower')
    divider = make_axes_locatable(axes)
    cax = divider.append_axes('right', size='5%', pad=0.05)
    cbar = fig.colorbar(imag, cax=cax)
    cbar.solids.set_rasterized(True)
    cbar.set_label('log(F_lambda)')
    #plt.show()
    plt.savefig(outname)

##################################################################################

def main():

    ''' Get density '''
    #rho = get_simple_density()
    #rho, nx, dx = load_box2(6)
    rho, nx, dx = load_sim(6, 'elena')
    plot_image(np.sum(rho, axis=0), 'density.png')
    gas_to_dust = 0.01
    rho = gas_to_dust * rho

    ''' set the background '''
    wavelengths = [3.55, 4.44, 7.87, 23.68, 70., 160., 250., 350., 500., 870.] # micron
    SED_background = [5.13272944e-18, #W/m2 micron
                      3.65441023e-18,
                      2.65229773e-17,
                      9.88593945e-16,
                      5.42549027e-16,
                      4.36738465e-16,
                      6.45974738e-17,
                      1.50578588e-17,
                      2.58597896e-18,
                      2.03943315e-20]

    # correct input lum for num pixels
    lum_px = SED_background[0] / (nx*nx)


    ''' extinction coeff, this is fixed for a specific dust mixture and wavelenght '''
    # (Where did I get this again?)
    kappa = 8.801e2 #cm2/g
    kappa = kappa*0.01**2/0.001 # m/kg

    ''' Calculate the extinction for every cell (1st transition) '''
    a = kappa * rho

    ''' Calculate the optical depth allong the line of sight for each pixel '''
    tau = np.sum(a, axis=0) * dx
    print 'tau', tau.min(), tau.max()
    
    ''' 'Ray tracing' along the grid direction '''
    image = np.full((nx,nx), lum_px)
    image = image * np.exp(-tau)
 
    ''' Plot image '''
    plot_image(image, 'dust_image.png')


if __name__=='__main__':
    main()
