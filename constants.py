'''
    LINE RADIATIVE TRANSFER

    file: constants.py
    Description: collection of predefined physical constants in various units
    Author: Tine Colman
'''

# Planck constant
h = 4.135667516e-15 #eV*s
h_ev = 4.135667516e-15 #eV*s
h_si = 6.626e-34 #J*s

# speed of light
c = 299792458.0 #m/s
c_si = 299792458.0 #m/s

# Boltzman constant
k_b = 8.617e-5 # eV/K
kb_ev = 8.617e-5 # eV/K
kb_cgs = 1.38064852e-16 # cm2 g s-2 K-1
kb_si = 1.38064852e-23 # m2 kg s-2 K-1

# gravitational constant
G_cgs = 6.67259e-8 # cm^3 g^-1 s^-2

# hydrogen mass
mH_cgs = 1.6737236e-24 # g
MH = 1.6737236e-27 # kg

# other
PC = 3.0857e16 # m              # parsec
AU_cgs = 1.49597871e13 # cm     # astronomical unit
yr_cgs = 3.1556926e7 # s        # year
MSUN = 1.989e30 # kg            # solar mass
eV = 6.241509e18 # J            # electron volt

