import sys
from matplotlib import pyplot as plt

from lineData import LineData
from calc_occupations import *
from constants_lineRT import *
from rt_functions import B_nu_ev
from species_info import load_species_info

''' Calculate the level populations with the different methods
    PARAMS:
      n_H = number density in H/cc
      T = temperature in K
      grad_v = velocity gradient in 1/s
      ld = lineData object for the molecule
      comp_fracs = fractions of the total density of each collision partner species
      abundance = chemical abundance of the molecule
      methods = list of methods to test
      plot = optional filename to plot the resulting x_i '''
def compare_methods(n_H, T, grad_v, ld, comp_fracs, abundance, methods, plot=None):

    # get number density of molecules in 1/m3
    n_H2 = n_H/2. * 1.e6 #1/m3
    n_molec = n_H2 * abundance #m-3

    # Set initial radiation field to the CMB
    rad_field_CMB = np.zeros((ld.num_lvls, ld.num_lvls))
    for i in range(ld.num_lvls):
        for j in range(ld.num_lvls):
            if ld.freq[i][j] != 0.0:
                #T_bg = 2.725 # cmb
                T_bg = 3. # cmb
                rad_field_CMB[i][j] = B_nu_ev(ld.freq[i][j], T_bg) # eV/s/Hz/m2

    # determine collision coefficients
    C = ld.calc_total_C(T, comp_fracs)

    # calculate level populations with the different methods
    sols = []
    for method in methods:
        if method=='partition':
            sol = calc_lvlpops_partion(T, ld.num_lvls, ld.g, ld.E)
        elif method=='LTE':
            sol = calc_lvlpops_coll(ld.num_lvls, C)
        elif method=='noRad':
            sol = calc_lvlpops_AC(ld.num_lvls, ld.A, C, n_H2)
        elif method=='NLTE':
            sol = calc_lvlpops_nonLTE(ld.num_lvls, ld.A, ld.B, C, n_H2, rad_field_CMB)
        elif method=='NLTE_iter':
            num_iter = 10
            sol = solve_lvlpops_nonLTE(ld, n_H2, comp_fracs, n_molec, T, rad_field_CMB, num_iter)
        elif 'LVG' in method:
            flag = False
            if method=='LVG':
                num_iter = 10
            elif method=='LVGred':
                num_iter = 10
                flag = True
            else:
                num_iter = int(method[-1:])
            sol, beta = solve_lvlpops_LVG(ld, n_H2, comp_fracs, n_molec, T, grad_v,
                                          rad_field_CMB, num_iter, flag_reduce=flag)
        else:
            print 'Unknown method', method
            sols = np.zeros(len(ld.num_lvls))
        sols.append(sol)

    if plot != None:
        # print a summary
        print 'PARAMS: n_H = {:1.1e} H/cc, T = {:.1f} K, grad_v = {:1.1e} s-1, abundance = {:1.1e}'.format(n_H, T, grad_v, abundance)
        header = 'Method:'
        total = 'Total: '
        for m in range(len(methods)):
            header = header + ' {:^11}'.format(methods[m])
            total = total + ' {: 1.8f}'.format(np.sum(sols[m]))
        print header

        for i in range(ld.num_lvls):
            line = 'lvl {:>3}'.format(i)
            for m in range(len(methods)):
                line = line + ' {: 1.8f}'.format(sols[m][i])
            print line

        # check if the sum is 1
        print total

        # plot the results
        plt.figure(1,(10,6))
        lvls = np.arange(ld.num_lvls)
        bar_width = 1.0/(len(methods)+3) #2barspace between levels, 1 barspace for sum(seperat)
        bar_space = bar_width/len(methods)
        for m in range(len(methods)):
            #plt.plot(lvls, sols[m], marker='s', label=methods[m])
            positions = lvls + m*(bar_width+bar_space)
            plt.bar(positions, sols[m], bar_width, label=methods[m])

        plt.xticks(np.arange(ld.num_lvls)+ len(methods)*bar_width/2., range(ld.num_lvls))
        plt.xlim(-bar_width,10-bar_width)
        plt.ylabel('Population')
        plt.xlabel('level')
        plt.legend()
        plt.savefig(plot)
        plt.close()

    return sols

''' Excitation temperature plot '''
def plot_excitation_T(n_H_array, T, grad_v, ld, comp_fracs, abundance, methods, outname, up, down):

    # calculate T_ex for each density, method and lvl
    T_ex_all = []
    for n in range(len(n_H_array)):
        print 'n_H =', n_H_array[n]
        sols = compare_methods(n_H_array[n], T, grad_v, ld, comp_fracs, abundance, methods, plot=None)
        # Determine the excitation temperature (up-down) for each method    
        T_ex = np.zeros(len(sols))
        for s in range(len(sols)):
            T_ex[s] = calc_T_ex(up, down, sols[s], ld.g, ld.E)
        T_ex_all.append(T_ex)

    T_ex_all = np.array(T_ex_all)

    # plot
    for m in range(len(methods)):
        plt.plot(n_H_array, T_ex_all[:,m], marker='s', label=methods[m])
        #markers = ['s', 'o', 'v', '^', '+', 'x']
    plt.plot((min(n_H_array),max(n_H_array)),(3.,3.))
    plt.plot((min(n_H_array),max(n_H_array)),(T,T))
    
    plt.xscale('log')
    plt.xlabel('density [H/cc]')
    plt.ylabel('temperature [K]')
    plt.legend()
    plt.title('{} %={:1.1e} ({}-{})'.format(species, abundance, up, down))
    plt.savefig(outname)
    plt.close()

''' Excitation temperature plot for different abundances '''
def compare_excitation_T(n_H_array, T, grad_v, ld, comp_fracs, abundance, compar, outname, up, down):

    # calculate T_ex for each density
    T_ex_all = []
    for n in range(len(n_H_array)):
        print 'n_H =', n_H_array[n]
        if compar=='abu':
            sols_1 = compare_methods(n_H_array[n], T, grad_v, ld, comp_fracs, abundance*0.1, ['LVG'])
            sols_2 = compare_methods(n_H_array[n], T, grad_v, ld, comp_fracs, abundance, ['LVG'])
            sols_3 = compare_methods(n_H_array[n], T, grad_v, ld, comp_fracs, abundance*10, ['LVG'])
        else: #compar==grad_v
            sols_1 = compare_methods(n_H_array[n], T, grad_v*0.1, ld, comp_fracs, abundance, ['LVG'])
            sols_2 = compare_methods(n_H_array[n], T, grad_v, ld, comp_fracs, abundance, ['LVG'])
            sols_3 = compare_methods(n_H_array[n], T, grad_v*10, ld, comp_fracs, abundance, ['LVG'])
        sols = [sols_1[0], sols_2[0], sols_3[0]]
        # Determine the excitation temperature (up-down) for each method    
        T_ex = np.zeros(len(sols))
        for s in range(len(sols)):
            T_ex[s] = calc_T_ex(up, down, sols[s], ld.g, ld.E)
        T_ex_all.append(T_ex)

    T_ex_all = np.array(T_ex_all)

    # plot

    if compar=='abu':
        plt.plot(n_H_array, T_ex_all[:,0], marker='s', label='abu {:1.1e}'.format(abundance*0.1))
        plt.plot(n_H_array, T_ex_all[:,1], marker='o', label='abu {:1.1e}'.format(abundance))
        plt.plot(n_H_array, T_ex_all[:,2], marker='v', label='abu {:1.1e}'.format(abundance*10))
    else:
        plt.plot(n_H_array, T_ex_all[:,0], marker='s', label='dv {:1.1e}'.format(grad_v*0.1))
        plt.plot(n_H_array, T_ex_all[:,1], marker='o', label='dv {:1.1e}'.format(grad_v))
        plt.plot(n_H_array, T_ex_all[:,2], marker='v', label='dv {:1.1e}'.format(grad_v*10))
    plt.plot((min(n_H_array),max(n_H_array)),(3.,3.))
    plt.plot((min(n_H_array),max(n_H_array)),(T,T))
    
    plt.xscale('log')
    plt.xlabel('density [H/cc]')
    plt.ylabel('temperature [K]')
    plt.legend()
    plt.savefig(outname)
    plt.close()

#-----test funcs------------------------------------------------------------------------------------
def inspect_lvlpops():

    # density in H/cc
    n_H = 1.e2

    # set the temperature and velocity gradient (fixed)
    T = 10.0 # K
    grad_v = 1.e-13 # 1/s

    # get line data and coefficients
    species = 'HCO+'
    comp_fracs, abundance = load_species_info(species)
    ld = LineData(species)
    #abundance = 1.e-7

    #methods = ['LVG1', 'LVG2', 'LVG3', 'LVG']
    methods=['LVG','LVGred']
    #methods = ['partition', 'LTE', 'noRad', 'NLTE', 'NLTE_iter', 'LVG']
    compare_methods(n_H, T, grad_v, ld, comp_fracs, abundance, methods,
                    plot='lvlpops_HCO_n{0:1.0e}_T{1:d}.png'.format(n_H, int(T)))

def inspect_T_ex(species):

    # density in H/cc
    n_H = np.logspace(0, 10, num=75)
    #n_H = np.logspace(2, 8, num=75)

    # set the temperature and velocity gradient (fixed)
    T = 10.0 # K
    grad_v = 1.e-13 # 1/s

    # get line data and coefficients
    comp_fracs, abundance = load_species_info(species)
    ld = LineData(species)

    #methods = ['LVG1', 'LVG2', 'LVG3', 'LVG']
    #methods = ['partition', 'LTE', 'noRad', 'NLTE', 'NLTE_iter', 'LVG']
    methods = ['LVG']

    outname = '{}_abu{:1.1e}_T{}_dv{:1.1e}.png'.format(species, abundance, T, grad_v)

    plot_excitation_T(n_H, T, grad_v, ld, comp_fracs, abundance, methods, 'T_ex_1-0'+outname, 1, 0)
    #compare_excitation_T(n_H, T, grad_v, ld, comp_fracs, abundance, 'abu',
    #                     'comp_T_ex_1-0_{}_T{}_dv{:1.1e}.png'.format(species, T, grad_v), 1, 0)
    #compare_excitation_T(n_H, T, grad_v, ld, comp_fracs, abundance, 'dv',
    #                     'comp_T_ex_1-0_{}_T{}_abu{:1.1e}.png'.format(species, T, abundance), 1, 0)

#---------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    #species = sys.argv[1]
    #inspect_T_ex(species)

    inspect_lvlpops()

